FROM amazonlinux:2

RUN mkdir /app

COPY target/app-1.0.jar /app/app.jar

RUN yum update -y

CMD [ "sleep", "1000s" ]
